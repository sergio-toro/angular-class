# Symfony + AngularJS

#### 1 Add useful Bundles to `composer.json`

```javascript
// App bundles
"jms/di-extra-bundle": "1.4.*@dev",               // Allow us to use DI (Dependency Injection) in Annotations
"jms/serializer-bundle": "0.13.*@dev",            // Work with Object Serialization/Unserialize
"friendsofsymfony/jsrouting-bundle": "1.1.*@dev", // Access to Route names with Javascript
"friendsofsymfony/user-bundle": "2.0.*@dev",      // Allows us to work with secure users

// Debug bundles
"raulfraile/ladybug-bundle": "~1.0",              // Allows us to use quick debug dump
"elao/web-profiler-extra-bundle": "2.3.*@dev",    // Profile extra info
"tracy/tracy": "dev-master",                      // Tracy error pages
"kutny/tracy-bundle": "dev-master"                // Symfony2 Tracy integration
```

#### 2 Register Bundles in `AppKernel.php`
```php
new JMS\DiExtraBundle\JMSDiExtraBundle($this),
new JMS\SerializerBundle\JMSSerializerBundle(),
new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
new FOS\UserBundle\FOSUserBundle(),

// Debug
new Kutny\TracyBundle\KutnyTracyBundle(),

// Degub only in dev/test mode
$bundles[] = new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle();
$bundles[] = new Elao\WebProfilerExtraBundle\WebProfilerExtraBundle();
```

#### 3 Configurar FOSUserBundle
```yml
# User class
fos_user:
    db_driver:     orm
    firewall_name: main
    # el namespace de tu clase 'User' específica
    user_class:    Angular\UserBundle\Entity\User
```
