(function() {

    'use strict';

    /**
     * This helper injects a function with the service
     * defined in the initServiceName string and returns service.prepare()
     * @param  {string} initServiceName Service name to inject
     * @return {mixed}                  Injected service response
     */
    window.interceptWith = function(initServiceName) {
        return [
            initServiceName,
            function(initService) {
                return initService.prepare();
            }
        ];
    };

    // Declare app level module which depends on filters, and services
    window.myApp = angular
    .module(
        'myApp',
        [
            // Angular core
            'ngRoute',
            'ngAnimate',
            'ngSanitize',
            
            // Angular libraries
            'angular-cache',
            
            // GPM App
            'myApp.filters',
            'myApp.services',
            'myApp.directives',
            'myApp.controllers'
        ]
    )
    .constant('REQUEST_START',  'REQUEST_START')
    .constant('REQUEST_END',    'REQUEST_END')
    .constant('NEW_ALERT',      'NEW_ALERT')

    .config([
        '$routeProvider',
        '$httpProvider',
        function($routeProvider, $httpProvider)
        {
            // Routing settings
            $routeProvider
                .when('/', {
                    templateUrl: '/bundles/angularcore/templates/home.html',
                    controller: 'HomeCtrl',
                })
                .when('/new-task', {
                    templateUrl: '/bundles/angularcore/templates/new-task.html',
                    controller: 'NewTaskCtrl',
                })
                .otherwise({
                    redirectTo: '/'
                })
            ;

            // Add XMLHttpRequest
            $httpProvider.defaults.headers.common["X-Requested-With" ] = "XMLHttpRequest";
        }
    ]);

})();