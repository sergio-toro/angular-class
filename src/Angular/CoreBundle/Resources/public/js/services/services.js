(function() {

    'use strict';

    angular
    .module(
        'myApp.services',
        []
    )
    .service('Route', [
        '$location',
        function($location)
        {
            /**
             * Gets Symfony2 exposed route
             * Usage: Route.api('homepage')
             * 
             * @param  {string} name    Route name defined in Annotations
             * @param  {object} params  Key->Value object with parameters
             * @return {string}         String route
             */
            this.api = function(path, params) {
                // We wrap Routing to allow easy unit test application
                // and use with Angular DI (Dependency Injection)
                return Routing.generate(path, params);
            };
        }
    ])

    /**
     * Loading service, emits events for loading directive 
     */
    .service('Loading', [
        '$rootScope',
        'REQUEST_START',
        'REQUEST_END',
        function($rootScope, REQUEST_START, REQUEST_END)
        {
            // Emits start loading event
            this.start = function()
            {
                $rootScope.$broadcast(REQUEST_START);
            };

            // Emits end loading event
            this.end = function()
            {
                $rootScope.$broadcast(REQUEST_END);
            };
        }
    ])

    /**
     * Alert service
     */
    .service('Alert', [
        '$rootScope',
        'NEW_ALERT',
        function($rootScope, NEW_ALERT)
        {
            function newAlert(type, message)
            {
                $rootScope.$broadcast(NEW_ALERT, {
                    type: type,
                    message: message
                });
            }

            // Emits start loading event
            this.success = function(message)
            {
                newAlert('success', message);
            };

            // Emits start loading event
            this.danger = function(message)
            {
                newAlert('danger', message);
            };

            // Emits start loading event
            this.info = function(message)
            {
                newAlert('info', message);
            };

            // Emits start loading event
            this.warning = function(message)
            {
                newAlert('warning', message);
            };
        }
    ])
    ;

})();