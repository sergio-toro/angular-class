(function() {
    'use strict';

    /* Controllers */

    angular
    .module(
        'myApp.controllers',
        []
    )
    .controller('HomeCtrl', [
        '$scope',
        '$http',
        'Route',
        'Loading',
        'Alert',
        function($scope, $http, Route, Loading, Alert)
        {
            $scope.tasks    = [];
            $scope.loading  = true;

            // We use $http service to store task in the server
            $http
                .get(
                    Route.api('get_tasks')  // Generates Symfony2 route
                )
                .success(function(data) {
                    $scope.loading  = false;
                    $scope.tasks    = data.tasks;
                })
                .error(function(data) {
                    $scope.loading = false;
                    Alert.danger('<strong>Ups!</strong> An error occurred, please try in a few minutes.');
                })
            ;

            // When a task updates
            $scope.change = function(index, key)
            {
                var update = {
                    id: $scope.tasks[index].id
                };
                update[key] = $scope.tasks[index][key];

                // Starts global loading
                Loading.start();

                $http
                    .post(
                        Route.api('update_task'),  // Generates Symfony2 route
                        {
                            task: update
                        }
                    )
                    .success(function(data) {
                        $scope.tasks[index] = data.task;
                        // Ends global loading
                        Loading.end();
                    })
                    .error(function(data) {
                        // Ends global loading
                        Loading.end();

                        Alert.danger('<strong>Ups!</strong> An error occurred updating your task.');
                    })
                ;
            };

            // Remove action
            $scope.remove = function(index)
            {
                // Start loading
                Loading.start();

                $http
                    .post(
                        Route.api('delete_task'),  // Generates Symfony2 route
                        {
                            id: $scope.tasks[index].id
                        }
                    )
                    .success(function(data) {
                        // Ends global loading
                        Loading.end();
                        // Remove task
                        $scope.tasks.splice(index, 1);

                        Alert.warning('Task deleted successfully.');
                    })
                    .error(function(data) {
                        // Ends global loading
                        Loading.end();

                        Alert.danger('<strong>Ups!</strong> An error occurred updating your task.');
                    })
                ;
            };
        }
    ])
    .controller('NewTaskCtrl', [
        '$scope',
        '$location',
        '$http',
        'Route',
        'Loading',
        'Alert',
        function($scope, $location, $http, Route, Loading, Alert)
        {
            $scope.task = {};
            $scope.task.color = 'default';

            /**
             * Saves new task.
             */
            $scope.save = function()
            {
                // Starts global loading
                Loading.start();

                // We use $http service to store task in the server
                $http
                    .post(
                        Route.api('new_task'),  // Generates Symfony2 route
                        {
                            'task': $scope.task // Task to be saved    
                        }
                    )
                    .success(function(data) {
                        // Ends global loading
                        Loading.end();

                        Alert.success("Your task has been created.");

                        $location.path('/');
                    })
                    .error(function(data) {
                        // Ends global loading
                        Loading.end();

                        Alert.danger("<strong>Ups!</strong> An error has ocurred creating new task.");
                    })
                ;
            };
        }
    ]);
})();