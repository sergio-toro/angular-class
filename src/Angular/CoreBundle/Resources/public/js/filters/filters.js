(function() {

    'use strict';

    /* Filters */

    angular
    .module(
        'myApp.filters',
        []
    )
    /**
     * Format UTC date, if format not provided returns time.fromNow()
     *
     * Usage (fromNow):
     * {{ '2013-11-09T20:05:10+0100' | moment }}
     * Usage (format):
     * {{ '2013-11-09T20:05:10+0100' | moment:'DD-MM-YYYY' }}
     */
    .filter(
        'moment',
        function()
        {
            return function(input, format)
            {
                // If input provided, then use moment.js to parse value
                if (input)
                {
                    return format ? moment.utc(input).format(format)
                        : moment.utc(input).fromNow()
                    ;
                }
            };
        }
    );
    
})();
