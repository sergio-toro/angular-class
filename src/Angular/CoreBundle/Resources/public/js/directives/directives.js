(function() {
    'use strict';

    /* Directives */
    angular
    .module(
        'myApp.directives',
        [
            'myApp.contenteditable'
        ]
    )
    /**
     * Checkbox directive (Pretty prints a nice checkbox)
     *
     * Usage: <checkbox ng-model="foo"></checkbox>
     */
    .directive("checkbox", [
        function ()
        {
            return {
                restrict: 'E',
                require: "ngModel",
                templateUrl: '/bundles/angularcore/templates/directives/checkbox.html',
                scope: true,
                compile: function(element, attr, transclude)
                {
                    // Sets binding between parent and child ngModel (Isolated scope)
                    attr.$set('ngModel', '$parent.'+attr.ngModel);

                    // Return link function
                    return function(scope, element, attr, ngModel)
                    {
                        // Render ngModel changes
                        ngModel.$render = function() {
                            scope.checked = ngModel.$viewValue;
                        };

                        scope.toggle = function() {
                            scope.checked = !scope.checked;
                            // Send to upper scope
                            ngModel.$setViewValue(scope.checked);
                        };
                    };
                }
            };
        }
    ])

    /**
     * Colorpicker directive (You can choose colors!)
     *
     * Usage: <colorpicker ng-model="color"></colorpicker>
     */
    .directive("colorpicker", [
        function ()
        {
            return {
                restrict: 'E',
                require: "ngModel",
                templateUrl: '/bundles/angularcore/templates/directives/colorpicker.html',
                scope: true,
                compile: function(element, attr, transclude)
                {
                    // Sets binding between parent and child ngModel (Isolated scope)
                    attr.$set('ngModel', '$parent.'+attr.ngModel);

                    //return link function
                    return function(scope, element, attr, ngModel)
                    {
                        scope.color  = 'default';
                        scope.colors = [
                            "green",
                            "pink",
                            "orange",
                            "red",
                            "blue",
                            "default"
                        ];

                        // Render ngModel changes
                        ngModel.$render = function() {
                            if (ngModel.$viewValue) {
                                scope.color = ngModel.$viewValue;
                            }
                        };
                        

                        // When color picked
                        scope.pick = function(color)
                        {
                            scope.color = color;
                            // Send changes to parent scope
                            ngModel.$setViewValue(scope.color);
                        };
                    };
                }
            };
        }
    ])
    
    /**
     * Tooltip directive, wrapper for jQuery bootstrap tooltip plugin
     *
     * Usage:
     * <span tooltip="Tooltip content!">My content</span>
     */
    .directive('tooltip', function() {
        return {
            link: function(scope, element, attrs, ctrl) {
                // Inits jQuery bootstrap tooltip plugin
                $(element).tooltip({
                    title: attrs.tooltip,
                });
            }
        };
    })
    
    /**
     * Loading directive, whatchs request start/end events, use with Loading service
     */
    .directive('loading', [ 'REQUEST_START', 'REQUEST_END', function(REQUEST_START, REQUEST_END) {
        return {
            restrict: "E",
            template: '<i class="fa-refresh fa-spin fa-2x"></i>',
            link: function(scope, element, attrs, ctrl)
            {
                // hide the element initially
                element.hide();

                scope.$on(REQUEST_START, function () {
                    // got the request start notification, show the element
                    element.show();
                });

                scope.$on(REQUEST_END, function () {
                    // got the request end notification, hide the element
                    element.hide();
                });
            }
        };
    }])
    
    /**
     * Show alerts
     */
    .directive('alerts', [ 'NEW_ALERT', function(NEW_ALERT) {
        return {
            restrict: "E",
            scope: {},
            templateUrl: '/bundles/angularcore/templates/directives/alerts.html',
            link: function(scope, element, attrs, ctrl)
            {
                scope.alerts = [];

                scope.$on(NEW_ALERT, function (event, alert) {
                    scope.alerts.push(alert);
                });

                // Remove alert
                scope.remove = function(index)
                {
                    scope.alerts.splice(index, 1);
                };
            }
        };
    }])

    .directive('confirm', [
        function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.confirm || "Are you sure?";
                    var clickAction = attr.onConfirm;
                    element.bind('click',function (event) {
                        if (window.confirm(msg))
                        {
                            scope.$eval(clickAction);
                        }
                    });
                }
            };
        }
    ])
    ;
})();