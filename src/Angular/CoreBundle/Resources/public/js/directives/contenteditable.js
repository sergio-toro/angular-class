(function() {
    'use strict';

    /* Directives */
    angular
    .module(
        'myApp.contenteditable', []
    )
    /**
     * Binds two way model changes to contenteditable HTML5 attribute 
     *
     * Usabe:
     * <h1 ng-model="myModel" contenteditable="true">My amazing editable title!</h1>
     */
    .directive('contenteditable', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                /**
                 * view -> model
                 * 
                 * When model updates in directive,
                 * need to propagate changes to parents
                 */
                elm.bind('blur', function() {
                    scope.$apply(function() {
                        ctrl.$setViewValue(elm.html());
                    });
                });

                /**
                 * model -> view
                 * 
                 * When model updates externally, 
                 * directive needs to update content
                 */
                ctrl.$render = function() {
                    elm.html(ctrl.$viewValue);
                };
            }
        };
    })
    ;
})();