<?php

namespace Angular\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage", options={ "expose"=true })
     */
    public function indexAction()
    {
        $appVersion = time();

        return $this->render(
            'AngularCoreBundle::index.html.twig', 
            [
                'appVersion'    => $appVersion
            ]
        );
    }

}
