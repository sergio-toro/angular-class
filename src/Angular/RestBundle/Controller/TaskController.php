<?php

namespace Angular\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Angular\RestBundle\Controller\Controller;
use Angular\CoreBundle\Entity\Task;

/**
 * @Route("/task")
 */
class TaskController extends Controller
{
    /**
     * @Route("/get_tasks", name="get_tasks", options={ "expose"=true })
     * @Method({"GET"})
     */
    public function getTasksAction()
    {
        // Gets user
        $user = $this->getCurrentUser();

        // If user not found, then do a 404 response
        if ($user===null)
        {
            return $this->jsonResponse([
                'error' => 'User not found'
            ], 404);    
        }
        else
        {
            // Set response
            return $this->jsonResponse([
                'success'   => true,
                'tasks'     => $user->getTasks()
            ]);
        }
    }

    
    /**
     * @Route("/new_task", name="new_task", options={ "expose"=true })
     * @Method({"POST"})
     */
    public function newTaskAction() 
    {
        // Retrieve task
        $task = $this->getRequestContent(
            'task', 
            'Angular\CoreBundle\Entity\Task'
        );

        // Gets user
        $user = $this->getCurrentUser();  

        // If user not found, then do a 404 response
        if ($user===null)
        {
            return $this->jsonResponse([
                'error' => 'User not found'
            ], 404);    
        }
        else
        {
            // Sets current user to task
            $task
                ->setUser($user)
                ->setDone(false)
            ;

            // Gets entity manager
            $em = $this->get('doctrine.orm.entity_manager');

            $em->persist($task);
            $em->flush();

            // Set response
            return $this->jsonResponse([
                'success'   => true,
                'task'      => $task
            ]);
        }
    }

    /**
     * @Route("/update_task", name="update_task", options={ "expose"=true })
     * @Method({"POST"})
     */
    public function updateTaskAction() 
    {
        // Retrieve new data
        $newData = $this->getRequestContent(
            'task', 
            'Angular\CoreBundle\Entity\Task'
        );

        // Retrieve task to update
        $task = $this
            ->get('doctrine')
            ->getRepository('Angular\CoreBundle\Entity\Task')
            ->findOneById(
                $newData->getId()
            )
        ;

        // Check if task exists
        if (!$task instanceof Task)
        {
            return $this->jsonResponse([
                'error' => 'Task not found'
            ], 404);    
        }
        else
        {
            // Gets new data as array
            foreach ($newData->getVars() as $key => $value) {
                // If new value not null
                if ($value!==null)
                {
                    // Makes getter and setter methods:
                    // 
                    // $key = ucfirst('id')
                    // $newData->{"get{$key}"}()    => $newData->getId()
                    // $task->{"set{$key}"}()       => $task->setId()
                    $key = ucfirst($key);

                    // Updates values
                    $task->{"set{$key}"}(
                        $newData->{"get{$key}"}()
                    );
                }
            }

            // Gets entity manager
            $em = $this->get('doctrine.orm.entity_manager');

            $em->persist($task);
            $em->flush();

            // Set response
            return $this->jsonResponse([
                'success'   => true,
                'task'      => $task
            ]);
        }
    }

    /**
     * @Route("/delete_task", name="delete_task", options={ "expose"=true })
     * @Method({"POST"})
     */
    public function deleteTaskAction() 
    {
        // Retrieve task to update
        $task = $this
            ->get('doctrine')
            ->getRepository('Angular\CoreBundle\Entity\Task')
            ->findOneById(
                $this->getRequestContent('id')
            )
        ;

        // Check if task exists
        if (!$task instanceof Task)
        {
            return $this->jsonResponse([
                'error' => 'Task not found'
            ], 404);    
        }
        else
        {
            // Gets entity manager
            $em = $this->get('doctrine.orm.entity_manager');

            $em->remove($task);
            $em->flush();

            // Set response
            return $this->jsonResponse([
                'success'   => true
            ]);
        }
    }

}
