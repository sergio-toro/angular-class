<?php
namespace Angular\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;

use JMS\Serializer\SerializationContext;


use Angular\UserBundle\Entity\User;

class Controller extends SymfonyController
{
    /**
     * Used to cache decode content
     */
    private $content = null;

    /**
     * When you use $http.post() method in AngularJS, it sends data to the server using
     * Request Payload, in other words, it sends JSON to the server.
     *
     * In PHP you can retrieve data using file_get_contents('php://input'), Symfony wraps
     * this in 'request' service: $this->get('request')->getContent()
     *
     * This method is a wrapper that decodes submitted JSON into array or an Entity
     * 
     * @param  string $key          If key provided, returns array index
     * @param  string $namespace    If namespace provided, tries to deserialize as a Object
     * @return mixed
     */ 
    public function getRequestContent($key = null, $namespace = null) 
    {
        // If content not decoded yet
        if ($this->content===null)
        {
            $this->content = json_decode(
                $this->get('request')->getContent(), 
                true // Recursively to array
            );
        }

        // If key provided
        if ($key!==null)
        {
            $content = isset($this->content[$key]) 
                ? $this->content[$key]  // Key exists
                : null                  // Key not found
            ;
        }
        else
        {
            $content = $this->content;
        }

        // If deserialize provided
        if ($namespace!==null) 
        {
            // Unserialize content into an Object
            $content = $this
                ->get('jms_serializer')
                ->deserialize(
                    json_encode($content),  // jms_serializer needs input to be a json
                    $namespace,             // Object namespace
                    'json'                  // Input formatting (json/xml)
                )
            ;
        }

        // Return full content
        return $content;
    }

    /**
     * Returns a Response formatted in JSON, serializes given response Array or Object
     * 
     * @param  mixed    $response   It could be an array or an Object
     * @param  integer  $status     Response HTTP Status code (default 200)
     * @return Response
     */
    public function jsonResponse($response, $status = 200)
    {
        $json = $this
            ->get('jms_serializer')
            ->serialize(
                $response,  // Serialize Array or Object
                'json',     // Serialize format
                SerializationContext::create()->enableMaxDepthChecks()
            )
        ;
        return new Response(
            $json, 
            $status,
            [
                'Content-Type' => 'application/json'
            ]
        );
    }

    /**
     * Gets current logged user
     * 
     * @return User|null Returns current logged in user
     */
    public function getCurrentUser()
    {
        // Get logged in user
        $user = $this
            ->get('security.context')
            ->getToken()
            ->getUser()
        ;

        // Returns user
        return $user instanceof User 
            ? $user 
            : null
        ;
    }
}
