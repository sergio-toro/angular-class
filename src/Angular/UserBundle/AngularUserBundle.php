<?php

namespace Angular\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AngularUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
