<?php

namespace Angular\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as FOSUser;
use JMS\Serializer\Annotation\Exclude;

use Angular\CoreBundle\Entity\Task;

/**
 * User
 *
 * @ORM\Entity()
 */
class User extends FOSUser
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Angular\CoreBundle\Entity\Task", mappedBy="user")
     * @ORM\OrderBy({"done" = "ASC", "createdAt" = "DESC"})
     **/
    protected $tasks;


    public function __construct()
    {
        parent::__construct();  
        $this->tasks = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id.
     *
     * @param integer $id the id
     *
     * @return self
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of tasks.
     *
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Sets the value of tasks.
     *
     * @param mixed $tasks the tasks
     *
     * @return self
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;

        return $this;
    }
}
